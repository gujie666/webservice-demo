package com.gujie.ws;

import com.alibaba.fastjson.JSONArray;
import org.springframework.stereotype.Component;

import javax.jws.WebService;
import java.util.List;

@Component
@WebService(name = "testApiService",
        targetNamespace = "http://andot.org/webservice/demo/server",
        endpointInterface = "com.gujie.ws.TestApiService",
        portName = "10000")
public class TestApiServiceImpl implements TestApiService {
    @Override
    public Person insertPersonInfo(String person) {
        List<Person> list = JSONArray.parseArray(person, Person.class);
        list.get(0).setName(list.get(0).getName() + "xixi");
        //TODO 逻辑处理
        return list.get(0);
    }
}