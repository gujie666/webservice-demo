package com.gujie.ws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(targetNamespace = "http://andot.org/webservice/demo/server")
public interface TestApiService {
    @WebMethod
    Person insertPersonInfo(@WebParam(name = "PERSON", targetNamespace = "http://andot.org/webservice/demo/server") String person);
}