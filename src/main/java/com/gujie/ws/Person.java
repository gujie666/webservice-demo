package com.gujie.ws;

import lombok.Data;

@Data
public class Person {
    private Integer id;
    private String name;
    private String niceName;
    private Integer age;
    private Double height;
}